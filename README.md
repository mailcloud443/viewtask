#adding repo





helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx

#creare ingress





kubectl create namespace nginx-ingress-sample

helm install my-nginx ingress-nginx/ingress-nginx \
--namespace nginx-ingress-sample \
--set controller.metrics.enabled=true \
--set-string controller.metrics.service.annotations."prometheus\.io/port"="10254" \
--set-string controller.metrics.service.annotations."prometheus\.io/scrape"="true"

#to check ingress
kubectl get service -n nginx-ingress-sample
